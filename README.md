
# BetGuess

Administrator tool designed for manage the database of a betting sport website.

- General stats about the website.
- Able to put the site in maintenance.
- Manage event (odds boosting).
- Manage every user register.
- Add a bet.

## Language used
- Java
- Java FXML
- MySQL
