package com.betguess.betguess;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.sql.*;

import javax.swing.JOptionPane;

public class PrimaryController {
	
    @FXML
    private TextField userbox;
    
    @FXML
    private PasswordField passbox;
    
    static Connection conn; //Static pour avoir accès a la base de données a partir du primary controller
    
    PreparedStatement PreparedStatement;
    
    ResultSet ResultSet;
    
    Stage topanel = new Stage(); //Creation d'un nouveau Stage pour l'ouverture de la seconde fenetre
    
    Scene scene;
	
	public void initialize() throws SQLException {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/betguess","root","");
	}
	
	// Fonction de vérification des informations entré dans les textfields
	public void LogButton(@SuppressWarnings("exports") ActionEvent event) {
		
		String username = userbox.getText().toString();
		String password = passbox.getText().toString();
		
		String verif = "SELECT * FROM users WHERE username = ? and password = ? and role_id = 4";
		
		try {
			PreparedStatement = PrimaryController.conn.prepareStatement(verif); //Verifie si les info rentré corresponde a la DB
            PreparedStatement.setString(1, username);
            PreparedStatement.setString(2, password);
            ResultSet = PreparedStatement.executeQuery();
            
        if(!ResultSet.next()){
            	JOptionPane.showMessageDialog(null,"Information invalide où vous n'êtes pas autorisé.","Information invalide",JOptionPane.WARNING_MESSAGE);
            }
            
        else {
    		String result = userbox.getText();
    		JOptionPane.showMessageDialog(null,"Bienvenue" + " " + result.toString());
    			
            Node node = (Node)event.getSource(); //Récupère la fenêtre actuelle
            topanel = (Stage) node.getScene().getWindow();
            topanel.close();// Ferme la fenetre actuelle
            scene = new Scene(FXMLLoader.load(getClass().getResource("panel.fxml"))); //Charge la fenetre suivante
            topanel.setScene(scene);
            topanel.show();
            }
        }
        catch(Exception e){
            System.out.println("Erreur SQL");
        }
	}
	
}
