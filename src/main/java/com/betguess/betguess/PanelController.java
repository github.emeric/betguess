package com.betguess.betguess;

import javafx.event.ActionEvent;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class PanelController {
	
	@FXML
	private Label regislabel;// Label du nombre d'enrengistré
	@FXML
	private Label lastreg; //Label nom du dernier enrengistrer label
	@FXML
	private Label pronolabel;//Label meilleur pronos
	@FXML
	private Label maintlabel; //Label maintenance
	@FXML
	private Button boutonmaint; //Bouton activation maintenance
	@FXML
	private Button desacbt; //Bouton désactivation de maintenance
	
	// Nombre d'inscript + dernier inscript
	PreparedStatement NUMnUSER; //Prepare l'instruction SQL
	ResultSet ResultSet; //Excute la requete SQL
	
	//Meilleur pronostiqueur
	PreparedStatement bestProno;
	ResultSet ResultSet2;
	
	//Maintenance
	PreparedStatement maint;
	int ResultSet3;	
	
	//Pour ouverture fenetre match
	Stage toMatchs = new Stage();
	Scene sceneMatchs;
	
	//Pour ouverture fenetre users
	Stage toUsers = new Stage();
	Scene sceneUsers;
	
	public void initialize() throws SQLException{
		
		// Nombre d'inscript + dernier inscript
		String user = "SELECT * FROM users";		
		
		NUMnUSER = PrimaryController.conn.prepareStatement(user);
		ResultSet = NUMnUSER.executeQuery();
		
		while(ResultSet.next()) {
			String nbr = ResultSet.getString("id");
			regislabel.setText(nbr);
			
			String lastr = ResultSet.getString("username");
			lastreg.setText(lastr);
		}
		
		//Meilleur pronostiqueur (TOTALEMENT BUGER)
		String prono = "SELECT MAX(CONCAT(bet_win, ' ', username)) AS username FROM users";	
		
		bestProno = PrimaryController.conn.prepareStatement(prono);
		ResultSet2 = bestProno.executeQuery();
		
		while(ResultSet2.next()) {
		String pronobest = ResultSet2.getString("username");
		pronolabel.setText(pronobest);
		}
	}
	
	//Maintenance
	//Boutton ON
	public void OnClick(@SuppressWarnings("exports") ActionEvent event) throws SQLException {
		String mainton = "UPDATE maintenance SET is_maint=1";
		
		maint = PrimaryController.conn.prepareStatement(mainton);
		ResultSet3 = maint.executeUpdate();
		maintlabel.setText("Maintenance actif");
		maintlabel.setTextFill(Color.web("#FF0000"));
	}
	
	//Boutton OFF
	public void OnClick2(@SuppressWarnings("exports") ActionEvent event) throws SQLException {
		String maintoff = "UPDATE maintenance SET is_maint=0";
		
		maint = PrimaryController.conn.prepareStatement(maintoff);
		ResultSet3 = maint.executeUpdate();
		maintlabel.setText("Maintenance desactivé");
		maintlabel.setTextFill(Color.web("#008000"));
	}
	
	//Ouverture d'ajoute de matchs
	public void goMatchAdd(@SuppressWarnings("exports") ActionEvent event) throws IOException {
		sceneMatchs = new Scene(FXMLLoader.load(getClass().getResource("m-ajout.fxml"))); //Charge la fenetre suivante
        toMatchs.setScene(sceneMatchs);
        toMatchs.show();
	}
	
	//Ouverture users
	public void goUser(@SuppressWarnings("exports") ActionEvent event) throws IOException {
		sceneUsers = new Scene(FXMLLoader.load(getClass().getResource("users.fxml")));
        toUsers.setScene(sceneUsers);
        toUsers.show();
	}
}

