package com.betguess.betguess;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class MatchController {

	@FXML
	TextField texthome;
	@FXML
	TextField textout;
	@FXML
	TextField textsport;
	
    PreparedStatement ajouteMatch;
    
    int ResultSet;
	
	public void butValid(@SuppressWarnings("exports") ActionEvent event) throws SQLException {
		
		String home_team = texthome.getText().toString();
		String outside_team = textout.getText().toString();
		String sport_id = textsport.getText().toString();
		
		String add = "INSERT INTO matchs (home_team, outside_team, sport_id) VALUES ('"+home_team+"', '"+outside_team+"', '"+sport_id+"')";
		
		ajouteMatch = PrimaryController.conn.prepareStatement(add);
		ResultSet = ajouteMatch.executeUpdate();
		
		JOptionPane.showMessageDialog(null,"Le match a bien été ajouté.");
	}
}
