module com.betguess.betguess {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
	requires javafx.base;
	requires javafx.graphics;
	requires java.desktop;

    opens com.betguess.betguess to javafx.fxml;
    exports com.betguess.betguess;
}